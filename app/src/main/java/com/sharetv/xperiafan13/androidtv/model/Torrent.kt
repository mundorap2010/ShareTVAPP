package com.sharetv.xperiafan13.androidtv.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Reader
import java.io.Serializable


data class Torrent(
        val title: String = "",
        val key: String = "",
        val img: String = "",
        val size: String = "",
        val quality: String = "",
        val synopsis: String = "",
        val trailer: String = "",
        val language: String = "",
        val uploader: String = "",
        val genre: List<String>
): Serializable {
    class Deserializer : ResponseDeserializable<Torrent> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, Torrent::class.java)
    }

    class ListDeserializer : ResponseDeserializable<List<Torrent>> {
        override fun deserialize(reader: Reader): List<Torrent> {
            val type = object : TypeToken<List<Torrent>>() {}.type
            return Gson().fromJson(reader, type)
        }
    }
}