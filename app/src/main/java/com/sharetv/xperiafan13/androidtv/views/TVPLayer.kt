package com.sharetv.xperiafan13.androidtv.views

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import android.webkit.*
import com.sharetv.xperiafan13.androidtv.R


class TVPLayer : AppCompatActivity() {

    private var key: String = ""
    private var webView: WebView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        key = intent!!.extras!!.get("data")!!.toString()
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_view_tv)
        webView = findViewById(R.id.webview)
        returnData(key)
    }
    @SuppressLint("SetJavaScriptEnabled", "JavascriptInterface")
    fun returnData(url: String) {
        webView!!.loadUrl(url)
        val webSettings = webView!!.settings
        webSettings.javaScriptEnabled = true
        webView!!.settings.domStorageEnabled = true
        webView!!.settings.loadWithOverviewMode = true
        webView!!.settings.useWideViewPort = true
        webView!!.webChromeClient = object : WebChromeClient() {}
        webView!!.webViewClient = object : WebViewClient() {
            @SuppressLint("NewApi")
            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                return null
            }
            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                handler.proceed()
            }
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                webView!!.loadUrl("""
                    javascript:(function() {
                        ${'$'}('body > :not(.video-container)').hide();
                        ${'$'}('.video-container').appendTo('body');
                    })()
                """.trimIndent())
            }
        }

    }
}
