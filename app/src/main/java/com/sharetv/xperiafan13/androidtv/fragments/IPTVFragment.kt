package com.sharetv.xperiafan13.androidtv.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.github.kittinunf.fuel.Fuel
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.utils.IPTVParser
import com.sharetv.xperiafan13.androidtv.views.TVPLayer
import com.sharetv.xperiafan13.androidtv.views.ViewMovieActivity
import kotlinx.android.synthetic.main.fragment_url.*

class IPTVFragment : Fragment() {

    private var serverType: String = "https://sv2.jaffmisshwedd.com/livelwtv/289.m3u8?token=Oq4hPy3pZ9dJD7ychnCHSQ&expires=1534648410"
    private var inTV: Boolean = false
    private var isTorrent: Boolean = false
    private var isMovie: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_iptv, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_tv.setOnClickListener {
            //inTV = true
            //IPTVParser(link.text.toString()).init()
        }
        btn.setOnClickListener {
            inTV = false
            val intent = Intent(context, TVPLayer::class.java)
            intent.putExtra("data", serverType)
            startActivity(intent)

        }
    }



}