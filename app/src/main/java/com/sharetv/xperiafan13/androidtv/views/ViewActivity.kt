package com.sharetv.xperiafan13.androidtv.views

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.kittinunf.fuel.Fuel
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.model.TV
import com.sharetv.xperiafan13.androidtv.utils.TVScrapper
import kotlinx.android.synthetic.main.activity_view.*

class ViewActivity : AppCompatActivity() {

    private var tv: TV? = null
    private var ringProgressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tv = intent.extras!!.get("data") as TV
        setContentView(R.layout.activity_view)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        window.setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.title = tv!!.title
        }
        Glide.with(baseContext).load(tv!!.img).apply(RequestOptions().centerCrop()).into(image)
        setList()
        setFab()
    }

    private fun createDialog() {
        val builder = AlertDialog.Builder(this@ViewActivity)
        builder.setTitle("Choose an option")
        val animals = arrayOf("TV", "Phone")
        builder.setItems(animals) { _, which ->
            when (which) {
                0 -> work(true)
                1 -> work(false)
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun work(flag: Boolean) {
        if (flag) {
            postData("http://cablegratis.tv/canal-en-vivo/${tv!!.key}")
        } else {
            val intent = Intent(this@ViewActivity, TVPLayer::class.java)
            intent.putExtra("data", "http://cablegratis.tv/canal-en-vivo/${tv!!.key}")
            startActivity(intent)
        }
        /*runOnUiThread {
            val temp = TVScrapper("http://cablegratis.tv/canal-en-vivo/${tv!!.key}", this, agent)
            temp.returnData()
            temp.setListener(object : TVScrapper.Listener {
                override fun onInterestingEvent(data: String) {
                    closeDialog()
                    if (flag) {
                        postData(data)
                    } else {
                        val intent = Intent(this@ViewActivity, TVPLayer::class.java)
                        intent.putExtra("data", data)
                        startActivity(intent)
                    }
                }
            })
        } */
    }

    private fun showLoadingDialog(msg: String) {
        ringProgressDialog = ProgressDialog.show(this, "Working ...", msg, true)
    }

    private fun closeDialog() {
        ringProgressDialog!!.dismiss()
    }

    private fun setFab() {
        Log.i("LATINO", tv!!.toString())
        val fab = findViewById<View>(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { _ ->
            showLoadingDialog("Getting Link")
            createDialog()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setList() {
        val webView = findViewById<WebView>(R.id.webview)
        webView.visibility = View.INVISIBLE
        if (tv!!.mx) {
            webView.loadUrl("https://mi.tv/mx/canales/${tv!!.list}")
        } else {
            webView.loadUrl("https://mi.tv/co/canales/${tv!!.list}")
        }
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                webView.loadUrl("""
                    javascript:(function() {
                        ${'$'}('body > :not(#listings)').hide("fast");
                        ${'$'}('#listings').appendTo('body');
                        ${'$'}('.channel-info').hide("fast");
                        ${'$'}('.desc').hide("fast");
                        ${'$'}('.btn-expand').hide("fast");
                    })()
                """.trimIndent())
                Handler().postDelayed({
                    spin_kit.visibility = View.INVISIBLE
                    webView.visibility = View.VISIBLE
                }, 3000)
            }
        }
    }

    private fun postData(key: String) {
        val code = PreferenceManager.getDefaultSharedPreferences(this).getString("code", "defaultStringIfNothingFound")
        Fuel.post("http://$code:5050/".replace("\\s".toRegex(), ""))
                .body("""
                {
                    "key": "$key"
                }
            """.trimIndent())
                .response { request, response, result -> }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
