package com.sharetv.xperiafan13.androidtv

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.Menu
import android.view.MenuItem
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.sharetv.xperiafan13.androidtv.fragments.*
import com.sharetv.xperiafan13.androidtv.utils.AutoTV
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.model.ProfileDrawerItem
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.sharetv.xperiafan13.androidtv.model.Version


class MainActivity : AppCompatActivity() {

    private var fragment_user: Fragment? = null
    private var toolbar: Toolbar? = null
    private var ringProgressDialog: ProgressDialog? = null
    var progressDialog: ProgressDialog? = null
    private var currentVersion: Double = 0.1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        setDrawer()
        fragment_user = TorrentFragment()
        changeFragment(fragment_user as TorrentFragment)
        PRDownloader.initialize(this)
        setupPermissions()
        checkUpdate()
    }

    private fun showLoadingDialog(msg: String) {
        ringProgressDialog = ProgressDialog.show(ContextThemeWrapper(this@MainActivity, R.style.AlertDialogCustom), "Conectando...", msg, true)
    }

    private fun closeDialog() {
        ringProgressDialog!!.dismiss()
    }

    private val TAG = "PermissionDemo"
    private val RECORD_REQUEST_CODE = 101
    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to record denied")
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                RECORD_REQUEST_CODE)
    }

    private fun profileHeader(): AccountHeader {
        return AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.hd)
                .addProfiles(
                        ProfileDrawerItem().withName("ShareBOX").withEmail("therealhack@gmail.com").withIcon(resources.getDrawable(R.drawable.user))
                )
                .withOnAccountHeaderListener { view, profile, currentProfile -> false }
                .build()
    }

    private fun setDrawer() {
        var currentPosition = 0
        val item2 = PrimaryDrawerItem().withName("Torrents")
        val item3 = PrimaryDrawerItem().withName("Web Scrap")
        val item4 = PrimaryDrawerItem().withName("Ajustes")
        DrawerBuilder().withActivity(this)
                .withToolbar(toolbar!!)
                .withAccountHeader(profileHeader())
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(item2, item3, item4)
                .withOnDrawerItemClickListener { _, position, _ ->
                    if (currentPosition != position) {
                        when (position) {
                            1 -> {
                                fragment_user = TorrentFragment()
                                changeFragment(fragment_user as TorrentFragment)
                            }
                            2 -> {
                                fragment_user = SeriesFragment()
                                changeFragment(fragment_user as SeriesFragment)
                            }
                            3 -> {
                                val manager = supportFragmentManager
                                val ft = manager.beginTransaction()
                                ft.replace(R.id.view, SettingsFragment())
                                ft.commitAllowingStateLoss()
                            }
                        }
                        currentPosition = position
                    }
                    false
                }
                .build()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.code -> showInput()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showInput() {
        var msg: String
        showLoadingDialog("Conectando a la televisión")
        doAsync {
            val ip = AutoTV(this@MainActivity).loopIP()
            uiThread {
                if (ip == "none") {
                    msg = "Android Box not found"
                    PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit().putString("code", "192.168.0.6" +
                            "").apply()
                } else {
                    msg = "Conexión establecida"
                    PreferenceManager.getDefaultSharedPreferences(this@MainActivity).edit().putString("code", ip).apply()
                }
                closeDialog()
                AlertDialog.Builder(ContextThemeWrapper(this@MainActivity, R.style.AlertDialogCustom)).setTitle("Conectado")
                        .setMessage(msg)
                        .setPositiveButton("Aceptar") { dialog, _ ->
                            dialog.dismiss()
                        }.show()
            }
        }
    }

    private fun changeFragment(fragment: Fragment) {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()
        ft.replace(R.id.view, fragment)
        ft.commitAllowingStateLoss()
    }

    private fun deleteContent(dir: File) {
        if (dir.isDirectory)
            for (child in dir.listFiles())
                deleteContent(child)

        dir.delete()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom)).setTitle("Salir")
                .setMessage("Limpiar archivos temporales?")
                .setPositiveButton("yes") { _, _ ->
                    deleteContent(File(Environment.getExternalStorageDirectory(), "ShareTV"))
                    finish()
                }.setNegativeButton("no") { _, _ ->
                    finish()
                }.show()
    }

    private fun setProgress() {
        progressDialog = ProgressDialog(this, R.style.AlertDialogCustom)
        progressDialog!!.max = 100
        progressDialog!!.setCancelable(false)
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setMessage("Espera...")
        progressDialog!!.setTitle("Descargando actualización")
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        progressDialog!!.show()

    }

    private fun checkUpdate() {
        "http://173.255.196.18:2828/version/".httpGet()
                .responseObject(Version.Deserializer()) { _, _, result ->
                    update(result)
                }
    }

    private fun update(result: Result<Version, FuelError>) {
        result.fold(success = {
           if (it.current > currentVersion) {
               AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom)).setTitle("Nueva Actualización")
                       .setMessage("Deseas descargar la nueva versión?")
                       .setPositiveButton("Si") { _, _ ->
                           downloadUpdate(it.url)
                       }.setNegativeButton("No") { _, _ ->

                       }.show()
           }
        }, failure = {
            Log.e("Error", String(it.errorData))
        })
    }

    private fun downloadUpdate(url: String) {
        setProgress()
        val path = File(Environment.getExternalStorageDirectory(), "ShareTV")
        PRDownloader.download(url, path.path, "update.apk")
                .build().setOnProgressListener {
                    val total = it.currentBytes * 100 / 16600000
                    progressDialog!!.progress = total.toInt()
                }.start(object : OnDownloadListener {
                    override fun onError(error: Error?) {
                    }

                    override fun onDownloadComplete() {
                        progressDialog!!.progress = 100
                        progressDialog!!.dismiss()
                        openInstall(File(Environment.getExternalStorageDirectory(), "ShareTV/update.apk"))
                    }
                })
    }

    private fun openInstall(path: File) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(Uri.fromFile(path), "application/vnd.android.package-archive")
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}

