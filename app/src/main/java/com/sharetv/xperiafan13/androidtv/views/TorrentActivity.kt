package com.sharetv.xperiafan13.androidtv.views

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.preference.PreferenceManager
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.kittinunf.fuel.Fuel
import com.github.se_bastiaan.torrentstream.StreamStatus
import com.github.se_bastiaan.torrentstream.TorrentOptions
import com.github.se_bastiaan.torrentstream.TorrentStream
import com.github.se_bastiaan.torrentstream.listeners.TorrentListener
import com.google.gson.Gson
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.model.Response
import com.sharetv.xperiafan13.androidtv.model.Torrent
import com.sharetv.xperiafan13.androidtv.utils.AsyncTaskLoadImage
import com.sharetv.xperiafan13.androidtv.utils.TorrentDownloader
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.torrent_activity.*
import java.io.File
import java.net.URL


class TorrentActivity : AppCompatActivity(), TorrentListener {

    private var torrent: Torrent? = null
    var progressDialog: ProgressDialog? = null
    private val TORRENT = "Torrent"
    private val folderMain = "ShareTV"
    val f = File(Environment.getExternalStorageDirectory(), folderMain)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        torrent = intent.extras!!.get("data") as Torrent
        setContentView(R.layout.torrent_activity)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
            supportActionBar!!.title = TORRENT
        }
        val img = findViewById<ImageView>(R.id.image)
        AsyncTaskLoadImage(img, this).execute(torrent!!.img)
        Glide.with(baseContext).load(torrent!!.img).apply(RequestOptions().centerCrop()).into(cover)
        setView()
        setFab()
    }

    private fun setView() {
        movie_title.text = torrent!!.title.capitalize()
        movie_genre.text = torrent!!.genre.joinToString(", ")
        movie_uploader.text = torrent!!.uploader
        movie_language.text = torrent!!.language
        movie_synopsis.text = torrent!!.synopsis
        trailer.setOnClickListener {
            watchYoutubeVideo(torrent!!.trailer.split("v=")[1])
        }
    }

    private fun watchYoutubeVideo(id: String) {
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
        val webIntent = Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=$id"))
        try {
            startActivity(appIntent)
        } catch (ex: ActivityNotFoundException) {
            startActivity(webIntent)
        }
    }

    private fun createDialog() {
        val builder = AlertDialog.Builder(ContextThemeWrapper(this@TorrentActivity, R.style.AlertDialogCustom))
        builder.setTitle("Choose an option")
        val animals = arrayOf(getString(R.string.tv_box), getString(R.string.local), getString(R.string.download))
        builder.setItems(animals) { _, which ->
            when (which) {
                0 -> work(true)
                1 -> work(false)
                2 -> {
                    createDirectory()
                    TorrentDownloader(this, torrent!!.title).download(torrent!!.key, f)
                }
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun work(flag: Boolean) {
        if (flag) {
            postData(torrent!!.key)
        } else {
            createDirectory()
            setProgress()
            val location = File(Environment.getExternalStorageDirectory(), "ShareTV")
            val torrentOptions = TorrentOptions.Builder()
                    .saveLocation(location)
                    .removeFilesAfterStop(true)
                    .build()

            val torrentStream = TorrentStream.init(torrentOptions)
            torrentStream.addListener(this)
            torrentStream.startStream(torrent!!.key)
        }
    }

    private fun setProgress() {
        progressDialog = ProgressDialog(this, R.style.AlertDialogCustom)
        progressDialog!!.max = 100
        progressDialog!!.setCancelable(false)
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setMessage("Espera...")
        progressDialog!!.setTitle("Guardando en cache")
        progressDialog!!.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        progressDialog!!.show()

    }

    private fun setFab() {
        val fab = findViewById<View>(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { _ ->
            createDialog()
        }
    }

    private fun postData(key: String) {
        var code = PreferenceManager.getDefaultSharedPreferences(this).getString("code", "none")
        if (code == "none") {
            code = PreferenceManager.getDefaultSharedPreferences(this).getString("pref_ip", "none")
        }
        if (code == "none") {
            showDialog()
        } else {
            Fuel.post("http://$code:5050/".replace("\\s".toRegex(), ""))
                    .body(Gson().toJson(Response(torrent = true, movie = false, title = torrent!!.title, key = key, img = torrent!!.img)))
                    .response { _, response, _ ->
                        Log.i("Response", response.toString())
                    }
        }
    }

    private fun showDialog() {
        val alertDialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AlertDialogCustom)).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage("Sin IP registrada")
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK"
        ) { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createDirectory() {
        if (!f.exists()) {
            f.mkdirs()
        }
    }

    override fun onStreamPrepared(torrent: com.github.se_bastiaan.torrentstream.Torrent) {
        Log.d(TORRENT, "OnStreamPrepared")
    }

    override fun onStreamStarted(torrent: com.github.se_bastiaan.torrentstream.Torrent) {
        Log.d(TORRENT, "onStreamStarted")
    }

    override fun onStreamError(torrent: com.github.se_bastiaan.torrentstream.Torrent, e: Exception) {
        Log.e(TORRENT, "onStreamError", e)
    }

    override fun onStreamReady(torrent: com.github.se_bastiaan.torrentstream.Torrent) {
        progressDialog!!.progress = 100
        progressDialog!!.dismiss()
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(torrent.videoFile.toString()))
        intent.setDataAndType(Uri.parse(torrent.videoFile.toString()), "video/mp4")
        startActivity(intent)
    }

    override fun onStreamProgress(torrent: com.github.se_bastiaan.torrentstream.Torrent, status: StreamStatus) {
        if (status.bufferProgress <= 100 && progressDialog!!.progress < 100 && progressDialog!!.progress != status.bufferProgress) {
            progressDialog!!.progress = status.bufferProgress
        }
    }

    override fun onStreamStopped() {
        Log.d(TORRENT, "onStreamStopped")
    }
}
