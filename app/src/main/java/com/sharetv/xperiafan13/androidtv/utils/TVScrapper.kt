package com.sharetv.xperiafan13.androidtv.utils

import android.annotation.SuppressLint
import android.net.http.SslError
import android.webkit.*
import android.widget.LinearLayout
import com.just.agentweb.AgentWeb
import com.sharetv.xperiafan13.androidtv.views.ViewActivity


class TVScrapper(private var url: String, private var ctx: ViewActivity, private var agent: LinearLayout) {

    var mListener: Listener? = null
    private var flag = true

    interface Listener {
        fun onInterestingEvent(data: String)
    }

    fun setListener(listener: Listener) {
        mListener = listener
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun returnData() {
        val webClient = object : WebViewClient() {
            @SuppressLint("NewApi")
            override fun shouldInterceptRequest(view: WebView, request: WebResourceRequest): WebResourceResponse? {
                val tempLink = request.url.toString()
                if (flag && tempLink.contains("m3u8") && !tempLink.contains("jwpltx.com")) {
                    flag = false
                    mListener!!.onInterestingEvent(tempLink)
                }
                return null
            }
            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                handler.proceed()
            }
        }
        AgentWeb.with(ctx)
                .setAgentWebParent(agent, LinearLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()
                .setWebViewClient(webClient)
                .setWebChromeClient (WebChromeClient())
                .createAgentWeb()
                .ready()
                .go(url)
    }

}
