package com.sharetv.xperiafan13.androidtv.utils

import android.app.NotificationManager
import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.os.Environment
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.frostwire.jlibtorrent.AlertListener
import com.frostwire.jlibtorrent.SessionManager
import com.frostwire.jlibtorrent.TorrentInfo
import com.frostwire.jlibtorrent.alerts.AddTorrentAlert
import com.frostwire.jlibtorrent.alerts.Alert
import com.frostwire.jlibtorrent.alerts.AlertType
import com.frostwire.jlibtorrent.alerts.BlockFinishedAlert
import com.sharetv.xperiafan13.androidtv.R
import java.io.File
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.CountDownLatch


class TorrentDownloader(context: Context, private var title: String) {
    val mNotifyManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    val mBuilder = NotificationCompat.Builder(context)

    private fun notification() {
        mBuilder.setContentTitle("Downloading ${title.substring(0, 10)}")
                .setContentText("Download in progress")
                .setSmallIcon(R.drawable.ic_play_circle_filled)
        mBuilder.setProgress(100, 0, false)
        mNotifyManager.notify(0, mBuilder.build())
    }

    fun download(data: String, directory: File) {
        val torrentUri = Uri.parse(data)
        val path = torrentUri.toString()
        AsyncTask.execute { downloadUsingNetworkUri(directory, URL(path)) }
    }


    private fun downloadTorrent(torrentFile: File, directory: File) {
        val s = SessionManager()
        val signal = CountDownLatch(1)
        s.addListener(object : AlertListener {
            override fun types(): IntArray? {
                return null
            }
            override fun alert(alert: Alert<*>) {
                val type = alert.type()
                when (type) {
                    AlertType.ADD_TORRENT -> {
                        (alert as AddTorrentAlert).handle().resume()
                        notification()
                    }
                    AlertType.BLOCK_FINISHED -> {
                        val a = alert as BlockFinishedAlert
                        val p = (a.handle().status().progress() * 100).toInt()
                        mBuilder.setProgress(100, p, false).setContentText("${a.handle().status().downloadRate() / 1000} Kb/s")
                        mNotifyManager.notify(0, mBuilder.build())
                    }
                    AlertType.TORRENT_FINISHED -> {
                        signal.countDown()
                        mBuilder.setContentText("Download complete")
                                .setProgress(0,0,false)
                    }
                    AlertType.TORRENT_ERROR -> {
                    }
                    else -> {
                    }
                }
            }
        })
        s.start()
        val ti = TorrentInfo(torrentFile)
        s.download(ti, directory)
        signal.await()
        s.stop()
    }

    /**
     * Download a torrent from the [torrentUrl] to the [downloadLocation] destination.
     */
    private fun downloadUsingNetworkUri(downloadLocation: File, torrentUrl: URL) {
        val connection = (torrentUrl.openConnection() as HttpURLConnection).apply {
            requestMethod = "GET"
            instanceFollowRedirects = true
        }
        connection.connect()
        if (connection.responseCode != 200) {
            Log.w("", "Unexpected response code returned from server: ${connection.responseCode}")
        }
        val data = connection.inputStream.readBytes()
        val photo = File(Environment.getExternalStorageDirectory(), "temp.torrent")
        if (photo.exists()) {
            photo.delete()
        }
        try {
            val fos = FileOutputStream(photo.path)
            fos.write(data)
            fos.close()
            downloadTorrent(photo, downloadLocation)
        } catch (e: java.io.IOException) {
            Log.e("Torrent", "Exception in photoCallback", e)
        }
    }
}

