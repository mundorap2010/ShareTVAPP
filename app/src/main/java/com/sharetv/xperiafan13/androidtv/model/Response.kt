package com.sharetv.xperiafan13.androidtv.model

import java.io.Serializable

data class Response (
        val title: String = "",
        val key: String = "",
        val img: String = "",
        val movie: Boolean = false,
        val torrent: Boolean = false
): Serializable