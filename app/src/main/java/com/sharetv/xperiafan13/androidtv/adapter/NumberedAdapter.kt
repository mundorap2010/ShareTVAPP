package com.sharetv.xperiafan13.androidtv.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.views.ViewActivity
import com.sharetv.xperiafan13.androidtv.model.TV

class NumberedAdapter(val data: List<TV>, val ctx: Context) : RecyclerView.Adapter<NumberedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tv = data[position]
        Glide.with(holder.cover.context).load(tv.img).apply(RequestOptions().centerCrop()).into(holder.cover)
        holder.cover.setOnClickListener {
            val intent= Intent(ctx, ViewActivity::class.java)
            intent.putExtra("data", tv)
            ctx.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cover: ImageView = itemView.findViewById(R.id.cover) as ImageView
    }
}