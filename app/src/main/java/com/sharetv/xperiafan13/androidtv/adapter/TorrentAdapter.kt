package com.sharetv.xperiafan13.androidtv.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sharetv.xperiafan13.androidtv.R
import com.sharetv.xperiafan13.androidtv.model.Torrent
import com.sharetv.xperiafan13.androidtv.views.TorrentActivity

class TorrentAdapter(val data: List<Torrent>, val ctx: Context) : RecyclerView.Adapter<TorrentAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: TorrentAdapter.ViewHolder, position: Int) {
        val tv = data[position]
        Glide.with(holder.cover.context).load(tv.img).apply(RequestOptions().centerCrop()).into(holder.cover)
        holder.size.text = tv.size
        holder.quality.text = tv.quality
        holder.title.text = tv.title.capitalize()
        holder.uploader.text = tv.uploader
        holder.more.setOnClickListener {
            val intent= Intent(ctx, TorrentActivity::class.java)
            intent.putExtra("data", tv)
            ctx.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.torrent_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cover: ImageView = itemView.findViewById(R.id.cover) as ImageView
        var more: Button = itemView.findViewById(R.id.more) as Button
        var title: TextView = itemView.findViewById(R.id.title) as TextView
        var uploader: TextView = itemView.findViewById(R.id.uploader) as TextView
        var size: TextView = itemView.findViewById(R.id.size) as TextView
        var quality: TextView = itemView.findViewById(R.id.quality) as TextView
    }
}