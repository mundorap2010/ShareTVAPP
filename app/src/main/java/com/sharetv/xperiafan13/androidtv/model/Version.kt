package com.sharetv.xperiafan13.androidtv.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import java.io.Reader
import java.io.Serializable

data class Version(
        val current: Double = 0.0,
        val url: String = ""
): Serializable {
    class Deserializer : ResponseDeserializable<Version> {
        override fun deserialize(reader: Reader) = Gson().fromJson(reader, Version::class.java)
    }
}