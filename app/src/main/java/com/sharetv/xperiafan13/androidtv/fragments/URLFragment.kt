package com.sharetv.xperiafan13.androidtv.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.github.kittinunf.fuel.Fuel
import com.sharetv.xperiafan13.androidtv.R
import kotlinx.android.synthetic.main.fragment_url.*
import android.content.Intent
import com.sharetv.xperiafan13.androidtv.views.ViewMovieActivity


class URLFragment : Fragment() {

    private var serverType: String = "none"
    private var inTV: Boolean = false
    private var isTorrent: Boolean = false
    private var isMovie: Boolean = true

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_url, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_tv.setOnClickListener {
            inTV = true
            returnURL(link.text.toString())
        }
        btn.setOnClickListener {
            inTV = false
            returnURL(link.text.toString())
        }
    }

    private fun returnURL(url: String) {

    }

    private fun postData(key: String) {
        val code = PreferenceManager.getDefaultSharedPreferences(context).getString("code", "defaultStringIfNothingFound")
        Fuel.post("http://$code:5050/".replace("\\s".toRegex(), ""))
                .body("""
                {
                    "key": "$key",
                    "movie": $isMovie,
                    "torrent": $isTorrent
                }
            """.trimIndent())
                .response { _, _, _ -> }
    }

}