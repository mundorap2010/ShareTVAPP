package com.sharetv.xperiafan13.androidtv.utils

import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.wifi.WifiManager
import android.util.Log
import android.net.ConnectivityManager
import java.io.IOException
import java.net.*


class AutoTV(private var ctx: Context) {

    fun loopIP(): String {
        var currentIp = "none"
        val wifiManager = ctx.applicationContext.getSystemService(WIFI_SERVICE) as WifiManager?
        val ipAddress = wifiManager!!.connectionInfo.ipAddress
        val ip = String.format("%d.%d.%d.%d", ipAddress and 0xff, ipAddress shr 8 and 0xff, ipAddress shr 16 and 0xff, ipAddress shr 24 and 0xff)
        var flag: Boolean
        for (i in 0..40) {
            val tIP = ip.split(".")
            val testIp = "${tIP[0]}.${tIP[1]}.${tIP[2]}.$i"
            flag = isURLReachable(testIp)
            if (flag) {
                currentIp = testIp
                return currentIp
            }
        }
        return currentIp
    }

    private fun isURLReachable(ip: String): Boolean {
        var isOpen = true
        try{
            val sa = InetSocketAddress(ip, 5050)
            val ss = Socket()
            ss.connect(sa, 1000)
            ss.close()
        }catch (ex: IOException) {
            isOpen = false
        }
        return isOpen
    }
}