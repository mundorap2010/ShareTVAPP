package com.sharetv.xperiafan13.androidtv.model

import com.google.gson.annotations.SerializedName


class Proper {

    @SerializedName("@tvg-logo")
    val logo: String = ""

    @SerializedName("@group-title")
    val title: String? = ""

}